<?php
add_action( 'init', 'hc_agua_taxonomies');
 
function hc_agua_taxonomies() {
    
  $labels = array(
    'name' => _x( 'Territorios', 'taxonomy general name' ),
    'singular_name' => _x( 'Territorio', 'taxonomy singular name' ),
    'search_items' =>  __( 'Buscar territorios' ),
    'popular_items' => __( 'Territorios populares' ),
    'all_items' => __( 'Todos los territorios' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Editar territorio' ), 
    'update_item' => __( 'Actualizar territorio' ),
    'add_new_item' => __( 'Agregar territorio' ),
    'new_item_name' => __( 'Agregar territorio' ),
    'separate_items_with_commas' => __( 'Separar territorios con comas' ),
    'add_or_remove_items' => __( 'Agregar o eliminar territorios' ),
    'choose_from_most_used' => __( 'Elegir de los territorios más usados' ),
    'menu_name' => __( 'Territorios' ),
  ); 
  
  register_taxonomy('territorios','post',array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    'rewrite' => array( 'slug' => 'territorio' ),
  ));
}

?>