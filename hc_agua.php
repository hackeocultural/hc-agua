<?php
/*
Plugin Name: HC Agua
Plugin URI: http://hackeocultural.org
Description: Ajustes personalizados del sitio web para Hackeo Cultural. 
Version: 0.3.20200513
Author: balamaqab
Author URI: http://gitlab.com/balamaqab
*/ 

require_once dirname( __FILE__ ) . '/taxonomies.php';
require_once dirname( __FILE__ ) . '/custom-styles.php';

?>